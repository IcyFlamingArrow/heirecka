# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="${PNV}.src"

require github [ release=${PN}-${PV} suffix=tar.gz ] cmake
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="The Programmers Solid 3D CAD Modeller"
DESCRIPTION="
OpenSCAD is not an interactive modeller. Instead it is something like a
3D-compiler that reads in a script file that describes the object and renders
the 3D model from this script file. This gives you (the designer) full control
over the modelling process and enables you to easily change any step in the
modelling process or make designs that are defined by configurable parameters.
OpenSCAD provides two main modelling techniques: First there is constructive
solid geometry (aka CSG) and second there is extrusion of 2D outlines.
Autocad DXF files can be used as the data exchange format for such 2D
outlines. In addition to 2D paths for extrusion it is also possible to read
design parameters from DXF files. Besides DXF files OpenSCAD can read and
create 3D models in the STL and OFF file formats."

HOMEPAGE="https://openscad.org/ ${HOMEPAGE}"

LICENCES=""
SLOT="0"
MYOPTIONS="
    gamepad [[ description = [ Enable support for Qt5Gamepad input driver ] ]]
    hidapi [[ description = [ Enable support for HIDAPI input driver ] ]]
    spacenavigator [[ description = [ Enable support for SpaceNavigator, a 3d input device ] ]]
"

QT_MIN_VER="5.4.0"

DEPENDENCIES="
    build:
        dev-util/itstool
        sys-devel/bison
        sys-devel/flex
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        3d-printing/lib3mf
        app-arch/libzip
        app-editors/QScintilla[>=2.8.0]
        dev-libs/boost[>=1.36]
        dev-libs/double-conversion:=
        dev-libs/glib:2[>=2.26]
        dev-libs/gmp:=
        dev-libs/mpfr:=
        dev-libs/libxml2:2.0[>=2.9]
        graphics/opencsg
        media-libs/fontconfig[>=2.8.0]
        media-libs/freetype:2[>=2.4.9]
        media-libs/glew
        sci-libs/CGAL
        sci-libs/eigen:3
        x11-dri/glu
        x11-dri/mesa
        x11-libs/cairo[>=1.14]
        x11-libs/harfbuzz[>=0.9.19]
        x11-libs/libX11
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtmultimedia:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        gamepad? ( x11-libs/qtgamepad:5 )
        hidapi? ( dev-libs/hidapi )
        spacenavigator? ( sci-libs/libspnav )
    test:
        dev-lang/python:*[>=3]
        media-gfx/ImageMagick
"

UPSTREAM_DOCUMENTATION="https://openscad.org/documentation.html"

CMAKE_SOURCE="${WORKBASE}/${PNV}"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DUSE_CCACHE:BOOL=OFF
    -DENABLE_CAIRO:BOOL=ON
    -DENABLE_QTDBUS:BOOL=ON
    -DHEADLESS:BOOL=OFF
    # Would download them during the build
    -DOFFLINE_DOCS:BOOL=OFF
    -DPROFILE:BOOL=OFF
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    GAMEPAD
    HIDAPI
    # This won't work, because there's no find_package call yet, but it's
    # unwritten anyway.
    'spacenavigator SPNAV'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DENABLE_TESTS:BOOL=ON -DENABLE_TESTS:BOOL=OFF'
)

openscad_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

openscad_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

