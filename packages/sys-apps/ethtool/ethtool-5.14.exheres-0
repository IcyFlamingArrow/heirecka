# Copyright 2009 Jan Meier
# Distributed under the terms of the GNU General Public License v2

require bash-completion

SUMMARY="Utility for controlling network drivers and hardware"
DESCRIPTION="
Particularly for wired Ethernet devices. It can be used to:
* Get identification and diagnostic information
* Get extended device statistics
* Control speed, duplex, autonegotiation and flow control for Ethernet devices
* Control checksum offload and other hardware offload features
* Control DMA ring sizes and interrupt moderation
* Control receive queue selection for multiqueue devices
* Upgrade firmware in flash memory
"
HOMEPAGE="https://www.kernel.org/pub/software/network/${PN}"
DOWNLOADS="mirror://kernel/software/network/ethtool/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-kernel/linux-headers
        virtual/pkg-config
    build+run:
        net-libs/libmnl
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-netlink
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "bash-completion bash-completion-dir ${BASHCOMPLETIONDIR}"
)

src_install() {
    default
}

